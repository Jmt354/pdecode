import numpy as np
import glob

# Combines all single A files into one total file

files = glob.glob("tst*.npy")
X = np.zeros((len(files),5))
i = 0
for file in files:
	dat = np.load(file)
	A = int(filter(str.isdigit,file))
	if np.isclose(dat[-1],0):
		print A
		continue
	X[i] = np.array(np.append(dat,A))
	i += 1
Y = np.vstack(X)
np.save('combtst',Y)

