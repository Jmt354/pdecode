import matplotlib
matplotlib.use('Agg')
import numpy as np
from matplotlib.patches import Wedge
from matplotlib.collections import PatchCollection
import matplotlib.pyplot as plt

fig, axarr = plt.subplots(4,4)
DATA = np.load('combtst.npy')
for dat in DATA:
	A = int(dat[4])
	i = (A-1) % 4
	j = (A-1) // 4
	r1 = dat[1]
	r2 = dat[2]
	alpha = dat[0]*180/np.pi
	patches = [Wedge((0,0),r2,0,alpha,width=(r2-r1))]
	colors = 100*np.random.rand(len(patches))
	p = PatchCollection(patches,alpha=.4)
	p.set_array(np.array(colors))
	axarr[i,j].locator_params(nbins=4)
	axarr[i,j].axis('equal')
	axarr[i,j].add_collection(p)
	axarr[i,j].autoscale()
	axarr[i,j].axis('equal')
	axarr[i,j].locator_params(nbins=4)
	axarr[i,j].grid(True)
	axarr[i,j].set_title("A = {}".format(A))

fig.suptitle('Minimum Eigenvalue Shapes')
fig.tight_layout()
fig.subplots_adjust(top=.85)
plt.savefig('A.png')
