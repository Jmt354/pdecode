from __future__ import print_function
from scipy.special import jv, yv, jvp, yvp
from scipy.optimize import brentq, minimize
import numpy as np
import matplotlib.pyplot as plt
import multiprocessing as mp

def lambda1(alpha,r1,r2):

	k = np.pi/alpha

	def detX(s):
	# Calculates the determinate for root finding
	# s = \sqrt{\lambda}
		s1 = r1*s
		s2 = r2*s
		p = jv(k,s1)*yv(k,s2) - jv(k,s2)*yv(k,s1)
		return p 

	def numzeros(Y):
	# Calculates the number of zeros in Y
		return np.sum(np.sign(Y[0:-1])*np.sign(Y[1:]) < 0)

	def onezeroint(a,b):
	# Shrinks b until the interval contains 1 zero
		S = np.linspace(a,b,1000)
		Y = detX(S)
		# Shrink b until we pass the zero
		while (numzeros(Y) > 1):
			b = b/2
			S = np.linspace(a,b,1000)
			Y = detX(S)
		# Grow b slowly until we see the zero
		while (numzeros(Y) < 1):
			b += .01
			S = np.linspace(a,b,1000)
			Y = detX(S)
		# Adjust b very slowly if we passed it
		while not(numzeros(Y) == 1):
			z = numzeros(Y)
			if z > 1:
				b -= .0001
			else:
				b += .0001
			S = np.linspace(a,b,1000)
			Y = detX(S)
		return b

	a = .001
	b = onezeroint(a,1000)
	
	# Now that there is only one zero, find it
	s = brentq(detX,a,b)
	return s*s #\lambda = \sqrt{\lambda}^2

def constAmin(A):
# Holding A constant, minimize lambda
	def lam1(r):
	# Shortcut function to calculate lambda from A,r1,r2
		print("Trying {}".format(r))
		# Constraints for the minimization
		if (    np.isclose(r[1]*r[1] - r[0]*r[0],0) 
		     or r[0] > r[1]
		   ):
			return 1e8 # Large number

		alpha = 2*A/(r[1]*r[1] - r[0]*r[0])
		return lambda1(alpha,r[0],r[1])

	# Bounds on r1 and r2
	bnds = ((1e-8, None), (1e-8, None))
	# Minimize by guessing a square shape, using truncated Newton method
	M = minimize(lam1, [6.6*np.sqrt(A),7.6*np.sqrt(A)], bounds=bnds,
				method='TNC', options={'maxiter':300})
	r1 = M.x[0]
	r2 = M.x[1]
	alpha = 2*A/(r2*r2 - r1*r1)
	print(M.success)
	print(M.message)
	return alpha,r1,r2,M.fun

