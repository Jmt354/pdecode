from __future__ import print_function
from scipy.special import jv, yv, jvp, yvp
from scipy.optimize import brentq, minimize
import numpy as np
import matplotlib.pyplot as plt
import multiprocessing as mp

# Gets the first n zeros with the mth set of functions
def lambdanm(alpha,r1,r2,n,m):

	k = m*np.pi/alpha

	def detX(s):
	# Calculates the determinate for root finding
	# s = \sqrt{\lambda}
		s1 = r1*s
		s2 = r2*s
		p = jv(k,s1)*yv(k,s2) - jv(k,s2)*yv(k,s1)
		return p 

	def numzeros(Y):
	# Calculates the number of zeros in Y
		return np.sum(np.sign(Y[0:-1])*np.sign(Y[1:]) < 0)

	def onezeroint(a,b):
	# Shrinks b until the interval contains 1 zero
		S = np.linspace(a,b,1000)
		Y = detX(S)
		# Shrink b until we pass the zero
		while (numzeros(Y) > 1):
			b = b/2
			S = np.linspace(a,b,1000)
			Y = detX(S)
		# Grow b slowly until we see the zero
		while (numzeros(Y) < 1 or b < a):
			b += .01
			S = np.linspace(a,b,1000)
			Y = detX(S)
		# Adjust b very slowly if we passed it
		while not(numzeros(Y) == 1):
			z = numzeros(Y)
			if z > 1:
				b -= .0001
			else:
				b += .0001
			S = np.linspace(a,b,1000)
			Y = detX(S)
		return b
	
	S = np.zeros(n)
	for ncurr in range(n):
		if ncurr == 0:
			a = .1
		else:
			a = b

		b = onezeroint(a,(ncurr+1)*1000)
	
		# Now that there is only one zero, find it
		s = brentq(detX,a,b)
		S[ncurr] = s
	return np.square(S) #\lambda = \sqrt{\lambda}^2

def lambda3(alpha,r1,r2):
	s1 = lambdanm(alpha,r1,r2,3,1)
	s2 = lambdanm(alpha,r1,r2,2,2)
	s3 = lambdanm(alpha,r1,r2,1,3)
	S = np.concatenate((s1,s2,s3))
	return np.sort(S)[2]


def constAmin(A):
# Holding A constant, minimize lambda
	def lam3(r):
	# Shortcut function to calculate lambda from A,r1,r2
		print("Trying {}".format(r))
		# Constraints for the minimization
		if (    np.isclose(r[1]*r[1] - r[0]*r[0],0) 
		     or r[0] > r[1]
		   ):
			return 1e8 # Large number

		alpha = 2*A/(r[1]*r[1] - r[0]*r[0])
		return lambda3(alpha,r[0],r[1])

	# Bounds on r1 and r2
	bnds = ((1e-8, None), (1e-8, None))
	# Minimize by guessing a square shape, using truncated Newton method
	M = minimize(lam3, [6.7*np.sqrt(A),7.7*np.sqrt(A)], bounds=bnds,
				method='TNC', options={'maxiter':300})
	r1 = M.x[0]
	r2 = M.x[1]
	alpha = 2*A/(r2*r2 - r1*r1)
	print(M.success)
	print(M.message)
	return alpha,r1,r2,M.fun

print(constAmin(1))
